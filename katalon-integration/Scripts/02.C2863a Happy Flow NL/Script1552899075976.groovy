import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('00. General Test Steps/00. Start_Application_Local'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('00. General Test Steps/00. Open_Sidemenu'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('00. General Test Steps/00. Select_Country'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('00. General Test Steps/00. Select_Location_NL'), [:], FailureHandling.STOP_ON_FAILURE)

'Field \'Adress/postalcode\''
Mobile.tap(findTestObject('android.widget.TextView0 - Brouwerijplein Enschede'), 0)

'Field \'Adress/Postalcode\''
Mobile.setText(findTestObject('android.widget.EditText0 - Address i.e. Amstelplein 10 (2)'), '8888', 0)

Mobile.hideKeyboard()

'Button \'Show Restaurant\''
Mobile.tap(findTestObject('android.widget.Button0 - SHOW RESTAURANTS (2)'), 0)

'Choice \'8888 Alpha\''
Mobile.tap(findTestObject('android.widget.LinearLayout3'), 0)

'Search button in menubar'
Mobile.tap(findTestObject('android.widget.ImageView6 (1)'), 0)

'Text Searchfield'
Mobile.setText(findTestObject('android.widget.EditText0 - Search for restaurants'), 'QA regression', 0)

Mobile.hideKeyboard()

'Restaurant selection'
Mobile.tap(findTestObject('android.widget.LinearLayout14 (1)'), 0)

'First menuchoice in menulist'
Mobile.tap(findTestObject('android.widget.LinearLayout25'), 0)

'basket button'
Mobile.tap(findTestObject('android.widget.RelativeLayout1 (2)'), 0)

'CHECKOUT button'
Mobile.tap(findTestObject('android.widget.Button0 - CHECKOUT (1)'), 0)

'Checkout adress'
Mobile.setText(findTestObject('android.widget.EditText0 - mandatory (1)'), 'Testersteeg 1', 0)

'Checkout postal code'
Mobile.setText(findTestObject('android.widget.EditText1 - 8888 (1)'), '8888 AA', 0)

'Checkout city'
Mobile.setText(findTestObject('android.widget.EditText2 - mandatory (1)'), 'Testershoven', 0)

'Checkout name'
Mobile.setText(findTestObject('android.widget.EditText3 - mandatory (1)'), 'Jan de Tester', 0)

'Checkout email'
Mobile.setText(findTestObject('android.widget.EditText4 - mandatory (1)'), 'jan@testen.nl', 0)

'Checkout phone number'
Mobile.setText(findTestObject('android.widget.EditText5 - mandatory (1)'), '0612345678', 0)

WebUI.callTestCase(findTestCase('00. General Test Steps/00. Payment_Method_Cash'), [:], FailureHandling.STOP_ON_FAILURE)

'Button \'Order and Pay\''
Mobile.tap(findTestObject('android.widget.Button0 - ORDER AND PAY (1)'), 0)

'button \'Closing order\' '
Mobile.tap(findTestObject('android.widget.Button0 - CLOSE (1)'), 0)

