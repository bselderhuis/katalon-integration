import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('00. General Test Steps/00. Start_Application_Local'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('00. General Test Steps/00. Open_Sidemenu'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('00. General Test Steps/00. Select_Country'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('00. General Test Steps/00. Select_Location_DE'), [:], FailureHandling.STOP_ON_FAILURE)

'Field \'Adress/Postalcode\''
Mobile.setText(findTestObject('android.widget.EditText0 - Adres b.v. Kirchstrae 1'), '88888', 0)

Mobile.hideKeyboard()

'Button \'Show Restaurant\''
Mobile.tap(findTestObject('android.widget.Button0 - ZOEK RESTAURANTS'), 0)

'Search button in menubar'
Mobile.tap(findTestObject('android.widget.ImageView6'), 0)

'Text Searchfield'
Mobile.setText(findTestObject('android.widget.EditText0 - Zoek op restaurantnaam'), 'Automated Mobile Testing', 0)

'Restaurant selection'
Mobile.tap(findTestObject('android.widget.LinearLayout14'), 0)

'First menuchoice in menulist'
Mobile.tap(findTestObject('android.widget.LinearLayout32 (1)'), 0)

'Choice 1 of extra\'s'
Mobile.tap(findTestObject('android.widget.ToggleButton0'), 0)

'Choice 2 of extra\'s'
Mobile.tap(findTestObject('android.widget.ToggleButton1'), 0)

'Choice 3 of extra\'s'
Mobile.tap(findTestObject('android.widget.ToggleButton2'), 0)

'Choice 4 of extra\'s'
Mobile.tap(findTestObject('android.widget.ToggleButton3 (1)'), 0)

'Choice 5 of extra\'s'
Mobile.tap(findTestObject('android.widget.ToggleButton4'), 0)

'Choice 6 of extra\'s'
Mobile.tap(findTestObject('android.widget.ToggleButton5'), 0)

'Choice 7 of extra\'s'
Mobile.tap(findTestObject('android.widget.ToggleButton6'), 0)

'\'Add to total (+)\' button'
Mobile.tap(findTestObject('android.widget.LinearLayout32 (2)'), 0)

'basket button'
Mobile.tap(findTestObject('android.widget.RelativeLayout1 (1)'), 0)

'\'+\' button in basket'
Mobile.tap(findTestObject('android.widget.ImageButton1'), 0)

'CHECKOUT button'
Mobile.tap(findTestObject('android.widget.Button0 - AFREKENEN'), 0)

'Checkout adress'
Mobile.setText(findTestObject('android.widget.EditText0 - verplicht'), 'Quality Strasse 1', 0)

'Checkout city'
Mobile.setText(findTestObject('android.widget.EditText2 - verplicht'), 'Testhausen', 0)

'Checkout name'
Mobile.setText(findTestObject('android.widget.EditText2 - verplicht (1)'), 'Heinz der Tester', 0)

'Checkout email'
Mobile.setText(findTestObject('android.widget.EditText3 - verplicht'), 'heinz@testmail.de', 0)

'Checkout phone number'
Mobile.setText(findTestObject('android.widget.EditText1 - verplicht'), '0612345678', 0)

WebUI.callTestCase(findTestCase('00. General Test Steps/00. Payment_Method_Cash'), [:], FailureHandling.STOP_ON_FAILURE)

'Button \'Order and Pay\''
Mobile.tap(findTestObject('android.widget.Button0 - NU KOPEN (1)'), 0)

'button \'Closing order\' '
Mobile.tap(findTestObject('android.widget.Button0 - SLUITEN'), 0)

